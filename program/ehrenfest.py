import random
import math
from statistics import median, mean, variance

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import pyplot
from scipy.interpolate import make_interp_spline

ball_amount = int(input("Nombre de balles : "))
ball_sqrt = math.sqrt(ball_amount)
balls_values = [False] * ball_amount

evolution = []
a = ball_amount

cycle_amount = int(input("Nombre de simulations : "))

# Simulation :

evolution.append(a)
for x in range(cycle_amount - 1):
    selected_ball = random.randrange(0, ball_amount, 1)
    balls_values[selected_ball] = not balls_values[selected_ball]
    a -= -1 + int(balls_values[selected_ball]) * 2
    evolution.append(a)

# Affichage du graphique :

fig = pyplot.gcf()
fig.set_size_inches(15, 4, forward=True)  # Valeurs à adapter en fonction du nombre de cycles

X_Y_Spline = make_interp_spline(range(cycle_amount), evolution)
X_ = np.linspace(0, cycle_amount - 1, 500)
plt.plot(X_, X_Y_Spline(X_), label="Boite A")
plt.axhline(y=ball_amount / 2, color='r', linestyle='-')
plt.axhline(y=ball_amount, color='black', linestyle='dotted')
plt.axhline(y=0, color='black', linestyle='dotted')
plt.legend()
plt.xlabel('Simulation')
plt.ylabel('Quantité')
plt.show()

fig.savefig('plot.png', dpi=200)

print(
    f"Min={min(evolution)} "
    f"Max={max(evolution)} "
    f"Moyenne={mean(evolution)} "
    f"Median={median(evolution)} "
    f"Variance={variance(evolution)}"
)
